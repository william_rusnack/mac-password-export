module Mac.Keychain.Parse where

import Control.Monad.Combinators
import Data.Time.Format
import Control.Lens
import Data.Bits
import Data.ByteString hiding (null)
import Data.Foldable hiding (null)
import Data.Functor
import Data.Word
import Mac.Keychain.Types
import Prelude hiding (null)
import Text.Ascii
import Text.Megaparsec
import Text.Megaparsec.Byte
import qualified Data.ByteString.Char8 as BS8
import qualified Data.ByteString as BS
import Data.Maybe


parseKeychain :: Ord e => ParsecT e ByteString m Keychain
parseKeychain = do
  p <-
    fmap BS8.unpack $
    chunk "keychain: " *> single doubleQuote *>
    takeWhile1P (Just "Keychain Path") (/= doubleQuote)
    <* single doubleQuote <* eol
  v <- chunk "version: " *> pt id uint32 <* eol
  k <-
    chunk "class: " *> takeWhileP Nothing isAlphaNum8 <* eol <*
    chunk "attributes:" <* eol >>= \case
      "genp" -> Kgenp <$> parseGENP
      "inet" -> Kinet <$> parseINET
      "0x0000000F" -> K0xF <$> parse0xF
      "0x00000010" -> K0x10 <$> parse0x10
      "0x80001000" -> K0x80001000 <$> parse0x80001000
      c -> fail $ "Non-existent keychain class: " <> show c
  pure $ Keychain
    { _path = p
    , _version = v
    , _key = k
    }

parseGENP :: Ord e => ParsecT e ByteString m GENP
parseGENP = do
  _ox7 <- space1 *> chunk "0x00000007" *> space *> pt id blob <* eol
  _ox8 <- space1 *> chunk "0x00000008" *> space *> pt null blob <* eol
  _acct <- space1 *> chunk "\"acct\"" *> space *> pt null blob <* eol
  _cdat <- space1 *> chunk "\"cdat\"" *> space *> pt id date <* eol
  _crtr <- space1 *> chunk "\"crtr\"" *> space *> pt null wordStr <* eol
  _cusi <- space1 *> chunk "\"cusi\"" *> space *> pt null uint32 <* eol
  _desc <- space1 *> chunk "\"desc\"" *> space *> pt null blob <* eol
  _gena <- space1 *> chunk "\"gena\"" *> space *> pt null blob <* eol
  _icmt <- space1 *> chunk "\"icmt\"" *> space *> pt null blob <* eol
  _invi <- space1 *> chunk "\"invi\"" *> space *> pt null uint32 <* eol
  _mdat <- space1 *> chunk "\"mdat\"" *> space *> pt id date <* eol
  _nega <- space1 *> chunk "\"nega\"" *> space *> pt null uint32 <* eol
  _prot <- space1 *> chunk "\"prot\"" *> space *> pt null blob <* eol
  _scrp <- space1 *> chunk "\"scrp\"" *> space *> pt null uint32 <* eol
  _svce <- space1 *> chunk "\"svce\"" *> space *> pt null blob <* eol
  _type <- space1 *> chunk "\"type\"" *> space *> pt null wordStr <* eol
  pure $ GENP {..}

parseINET :: Ord e => ParsecT e ByteString m INET
parseINET = do
  _ox7 <- space1 *> chunk "0x00000007" *> space *> pt id blob <* eol
  _ox8 <- space1 *> chunk "0x00000008" *> space *> pt id blob <* eol
  _acct <- space1 *> chunk "\"acct\"" *> space *> pt null blob <* eol
  _atyp <- space1 *> chunk "\"atyp\"" *> space *> pt id blob <* eol
  _cdat <- space1 *> chunk "\"cdat\"" *> space *> pt id date <* eol
  _crtr <- space1 *> chunk "\"crtr\"" *> space *> pt null wordStr <* eol
  _cusi <- space1 *> chunk "\"cusi\"" *> space *> pt null uint32 <* eol
  _desc <- space1 *> chunk "\"desc\"" *> space *> pt null blob <* eol
  _icmt <- space1 *> chunk "\"icmt\"" *> space *> pt null blob <* eol
  _invi <- space1 *> chunk "\"invi\"" *> space *> pt null uint32 <* eol
  _mdat <- space1 *> chunk "\"mdat\"" *> space *> pt id date <* eol
  _nega <- space1 *> chunk "\"nega\"" *> space *> pt null uint32 <* eol
  _path <- space1 *> chunk "\"path\"" *> space *> pt null blob <* eol
  _port <- space1 *> chunk "\"port\"" *> space *> pt id uint32 <* eol
  _prot <- space1 *> chunk "\"prot\"" *> space *> pt null blob <* eol
  _ptcl <- space1 *> chunk "\"ptcl\"" *> space *> pt id uint32 <* eol
  _scrp <- space1 *> chunk "\"scrp\"" *> space *> pt null uint32 <* eol
  _sdmn <- space1 *> chunk "\"sdmn\"" *> space *> pt null blob <* eol
  _srvr <- space1 *> chunk "\"srvr\"" *> space *> pt id blob <* eol
  _type <- space1 *> chunk "\"type\"" *> space *> pt null wordStr <* eol
  pure $ INET {..}

parse0xF :: Ord e => ParsecT e ByteString m OxF
parse0xF = do
  _ox0 <- space1 *> chunk "0x00000000" *> space *> pt id uint32 <* eol
  _ox1 <- space1 *> chunk "0x00000001" *> space *> pt id blob <* eol
  _ox2 <- space1 *> chunk "0x00000002" *> space *> pt id blob <* eol
  _ox3 <- space1 *> chunk "0x00000003" *> space *> pt id uint32 <* eol
  _ox4 <- space1 *> chunk "0x00000004" *> space *> pt id uint32 <* eol
  _ox5 <- space1 *> chunk "0x00000005" *> space *> pt id uint32 <* eol
  _ox6 <- space1 *> chunk "0x00000006" *> space *> pt id blob <* eol
  _ox7 <- space1 *> chunk "0x00000007" *> space *> pt id blob <* eol
  _ox8 <- space1 *> chunk "0x00000008" *> space *> pt id blob <* eol
  _ox9 <- space1 *> chunk "0x00000009" *> space *> pt id uint32 <* eol
  _oxA <- space1 *> chunk "0x0000000A" *> space *> pt id uint32 <* eol
  _oxB <- space1 *> chunk "0x0000000B" *> space *> pt id uint32 <* eol
  _oxC <- space1 *> chunk "0x0000000C" *> space *> pt id blob <* eol
  _oxD <- space1 *> chunk "0x0000000D" *> space *> pt id blob <* eol
  _oxE <- space1 *> chunk "0x0000000E" *> space *> pt id uint32 <* eol
  _oxF <- space1 *> chunk "0x0000000F" *> space *> pt id uint32 <* eol
  _ox10 <- space1 *> chunk "0x000000010" *> space *> pt id uint32 <* eol
  _ox11 <- space1 *> chunk "0x000000011" *> space *> pt id uint32 <* eol
  _ox12 <- space1 *> chunk "0x000000012" *> space *> pt id uint32 <* eol
  _ox13 <- space1 *> chunk "0x000000013" *> space *> pt id uint32 <* eol
  _ox14 <- space1 *> chunk "0x000000014" *> space *> pt id uint32 <* eol
  _ox15 <- space1 *> chunk "0x000000015" *> space *> pt id uint32 <* eol
  _ox16 <- space1 *> chunk "0x000000016" *> space *> pt id uint32 <* eol
  _ox17 <- space1 *> chunk "0x000000017" *> space *> pt id uint32 <* eol
  _ox18 <- space1 *> chunk "0x000000018" *> space *> pt id uint32 <* eol
  _ox19 <- space1 *> chunk "0x000000019" *> space *> pt id uint32 <* eol
  _ox1A <- space1 *> chunk "0x00000001A" *> space *> pt id uint32 <* eol
  pure $ OxF {..}
  
parse0x10 :: Ord e => ParsecT e ByteString m Ox10
parse0x10 = do
  _ox0 <- space1 *> chunk "0x00000000" *> space *> pt id uint32 <* eol
  _ox1 <- space1 *> chunk "0x00000001" *> space *> pt id blob <* eol
  _ox2 <- space1 *> chunk "0x00000002" *> space *> pt id blob <* eol
  _ox3 <- space1 *> chunk "0x00000003" *> space *> pt id uint32 <* eol
  _ox4 <- space1 *> chunk "0x00000004" *> space *> pt id uint32 <* eol
  _ox5 <- space1 *> chunk "0x00000005" *> space *> pt id uint32 <* eol
  _ox6 <- space1 *> chunk "0x00000006" *> space *> pt id blob <* eol
  _ox7 <- space1 *> chunk "0x00000007" *> space *> pt id blob <* eol
  _ox8 <- space1 *> chunk "0x00000008" *> space *> pt id blob <* eol
  _ox9 <- space1 *> chunk "0x00000009" *> space *> pt id uint32 <* eol
  _oxA <- space1 *> chunk "0x0000000A" *> space *> pt id uint32 <* eol
  _oxB <- space1 *> chunk "0x0000000B" *> space *> pt id uint32 <* eol
  _oxC <- space1 *> chunk "0x0000000C" *> space *> pt id blob <* eol
  _oxD <- space1 *> chunk "0x0000000D" *> space *> pt id blob <* eol
  _oxE <- space1 *> chunk "0x0000000E" *> space *> pt id uint32 <* eol
  _oxF <- space1 *> chunk "0x0000000F" *> space *> pt id uint32 <* eol
  _ox10 <- space1 *> chunk "0x000000010" *> space *> pt id uint32 <* eol
  _ox11 <- space1 *> chunk "0x000000011" *> space *> pt id uint32 <* eol
  _ox12 <- space1 *> chunk "0x000000012" *> space *> pt id uint32 <* eol
  _ox13 <- space1 *> chunk "0x000000013" *> space *> pt id uint32 <* eol
  _ox14 <- space1 *> chunk "0x000000014" *> space *> pt id uint32 <* eol
  _ox15 <- space1 *> chunk "0x000000015" *> space *> pt id uint32 <* eol
  _ox16 <- space1 *> chunk "0x000000016" *> space *> pt id uint32 <* eol
  _ox17 <- space1 *> chunk "0x000000017" *> space *> pt id uint32 <* eol
  _ox18 <- space1 *> chunk "0x000000018" *> space *> pt id uint32 <* eol
  _ox19 <- space1 *> chunk "0x000000019" *> space *> pt id uint32 <* eol
  _ox1A <- space1 *> chunk "0x00000001A" *> space *> pt id uint32 <* eol
  pure $ Ox10 {..}

parse0x80001000 :: Ord e => ParsecT e ByteString m Ox80001000
parse0x80001000 = do
  _alis <- space1 *> chunk "\"alis\"" *> space *> pt id blob <* eol
  _cenc <- space1 *> chunk "\"cenc\"" *> space *> pt id uint32 <* eol
  _ctyp <- space1 *> chunk "\"ctyp\"" *> space *> pt id uint32 <* eol
  _hpky <- space1 *> chunk "\"hpky\"" *> space *> pt id blob <* eol
  _issu <- space1 *> chunk "\"issu\"" *> space *> pt id blob <* eol
  _labl <- space1 *> chunk "\"labl\"" *> space *> pt id blob <* eol
  _skid <- space1 *> chunk "\"skid\"" *> space *> pt null blob <* eol
  _snbr <- space1 *> chunk "\"snbr\"" *> space *> pt id blob <* eol
  _subj <- space1 *> chunk "\"subj\"" *> space *> pt id blob <* eol
  pure $ Ox80001000 {..}


pt :: Ord e
   => (ParsecT e ByteString m a -> ParsecT e ByteString m b)
   -> (ByteString, ParsecT e ByteString m a)
   -> ParsecT e ByteString m b
pt f (n,p) = chunk ("<" <> n <> ">=") *> f p

null :: Ord e => ParsecT e ByteString m a -> ParsecT e ByteString m (Maybe a)
null p = try (chunk "<NULL>" $> Nothing) <|> Just <$> p

date :: Ord e => (ByteString, ParsecT e ByteString m Date)
date = ("timedate",) do
  dateChars <- fmap BS8.unpack $
    (try (chunk "0x" *> many hexDigitChar *> space1) <|> pure ()) *>
    single (ascii '"') *>
    takeP Nothing 8 <* -- year month day then ???
    takeP Nothing 6 <*
    chunk "Z\\000"
  Date <$> parseTimeM True defaultTimeLocale "%Y%m%d" dateChars

blob :: Ord e => (ByteString, ParsecT e ByteString m Blob)
blob = ("blob",) do
  hexval <- try $ optional hex <* space
  blobStr <- fmap (fromMaybe "") $ optional $ utf8
--    takeWhileP Nothing (/= doubleQuote) *>
--    single doubleQuote *>
--    takeWhileP Nothing (/= doubleQuote) <*
--    single doubleQuote
  pure case hexval of
    Just hv -> HexBlob hv blobStr
    Nothing -> Blob blobStr

utf8 :: Ord e => ParsecT e ByteString m ByteString
utf8 = do
  _ <- single doubleQuote
  mconcat <$> flip manyTill (single doubleQuote) do
    rcs <- takeWhileP Nothing (\c -> c /= ascii '\\' && c /= ascii '"')
    oc <- try $ optional $ oct 3
    pure case oc of
      Just c -> rcs `BS.snoc` c
      Nothing -> rcs

uint32 :: forall e m a. (Num a, Bits a, Ord e) => (ByteString, ParsecT e ByteString m a)
uint32 = ("uint32", hex)

hex :: forall e m a. (Num a, Ord e) => ParsecT e ByteString m a
hex = do
  cs <- chunk "0x" *> takeWhile1P (Just "Hex Values") isAlphaNum8
  foldlM (\n c -> (n * 0x10 +) <$> askii2num c) 0 $ unpack cs
  where
    askii2num x = fromUpHexDigit8 x & \case
      Just n -> pure n
      Nothing -> fail $ "While parsing hex the char was found: " <> show x

oct :: forall e m a. (Num a, Ord e) => Int -> ParsecT e ByteString m a
oct numChars = do
  cs <- chunk "\\" *> takeP Nothing numChars
  foldlM (\n c -> (n * 8 +) <$> askii2num c) 0 $ BS8.unpack cs
  where
    askii2num x = fromOctDigit x & \case
      Just n -> pure n
      Nothing -> fail $ "While parsing oct the char was found: " <> show x

wordStr :: (Num a, Bits a, Ord e) => (ByteString, ParsecT e ByteString m a)
wordStr = ("uint32",) do
  bytes <-
    optional (hex @_ @_ @Word *> space1) *>
    single doubleQuote *>
    -- takeWhile1P (Just "Retreiving characters") (/= doubleQuote) <*
    takeP Nothing 4 <*
    single doubleQuote
  pure $ ifoldrOf
    (to unpack . ifolded)
    (\i a r -> shiftL (fromIntegral a) ((3-i)*8) + r)
    zeroBits
    bytes

class FromChar a where fromChar :: Char -> a
instance FromChar Char where fromChar = id
instance FromChar Word8 where fromChar = ascii

doubleQuote :: Word8
doubleQuote = fromChar '"'

(>>>) :: (a -> b) -> (b -> c) -> (a -> c)
(>>>) = flip (.)
infixr 9 >>>

