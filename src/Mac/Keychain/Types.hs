{-# OPTIONS_GHC -fno-warn-missing-fields #-}
module Mac.Keychain.Types where

import Text.Ascii (ascii)
import Data.ByteString.Internal
import Data.Serializer
import Data.Int
import Data.Word
import Control.Lens
import Data.HashMap.Strict as HM
import Data.Time.Clock
import Data.Hashable
import GHC.Generics
import Data.Bits
import Data.String
import Data.Foldable as F


data Blob
  = Blob ByteString
  | HexBlob Integer ByteString
  deriving (Show, Eq, Ord, Generic)
instance Hashable Blob
makePrisms ''Blob

instance IsString Blob where
  fromString = Blob . fromString

data Date = Date UTCTime
  deriving (Show, Eq, Ord)

data AttributeType
  = BlobA ByteString
  | SInt32 Int32
  | Timedate Date
  | UInt32 Word32
makePrisms ''AttributeType

newtype Word32Str = Word32Str Word32
  deriving (Eq, Ord, Serializable, Bits, Num, Real, Enum, Integral)
instance Show Word32Str where
  show = reverse . fmap w2c . toBytes 

instance IsString Word32Str where
  fromString = F.foldl' (\w c -> fromIntegral (ascii c) + shiftL w 8) 0

data GENP = GENP 
    { _ox7  :: Blob
    , _ox8  :: Maybe Blob
    , _acct :: Maybe Blob
    , _cdat :: Date
    , _crtr :: Maybe Word32Str
    , _cusi :: Maybe Int32
    , _desc :: Maybe Blob
    , _gena :: Maybe Blob
    , _icmt :: Maybe Blob
    , _invi :: Maybe Int32
    , _mdat :: Date
    , _nega :: Maybe Int32
    , _prot :: Maybe Blob
    , _scrp :: Maybe Int32
    , _svce :: Maybe Blob
    , _type :: Maybe Word32
    } deriving (Show, Eq, Ord)
makeFields ''GENP

data INET = INET
    { _ox7 :: Blob
    , _ox8 :: Blob
    , _acct :: Maybe Blob
    , _atyp :: Blob
    , _cdat :: Date
    , _crtr :: Maybe Word32Str
    , _cusi :: Maybe Int32
    , _desc :: Maybe Blob
    , _icmt :: Maybe Blob
    , _invi :: Maybe Int32
    , _mdat :: Date
    , _nega :: Maybe Int32
    , _path :: Maybe Blob
    , _port :: Word32
    , _prot :: Maybe Blob
    , _ptcl :: Word32
    , _scrp :: Maybe Int32
    , _sdmn :: Maybe Blob
    , _srvr :: Blob
    , _type :: Maybe Word32
    }
makeFields ''INET

data OxF = OxF
    { _ox0 :: Word32
    , _ox1 :: Blob
    , _ox2 :: Blob
    , _ox3 :: Word32
    , _ox4 :: Word32
    , _ox5 :: Word32
    , _ox6 :: Blob
    , _ox7 :: Blob
    , _ox8 :: Blob
    , _ox9 :: Word32
    , _oxA :: Word32
    , _oxB :: Word32
    , _oxC :: Blob
    , _oxD :: Blob
    , _oxE :: Word32
    , _oxF :: Word32
    , _ox10 :: Word32
    , _ox11 :: Word32
    , _ox12 :: Word32
    , _ox13 :: Word32
    , _ox14 :: Word32
    , _ox15 :: Word32
    , _ox16 :: Word32
    , _ox17 :: Word32
    , _ox18 :: Word32
    , _ox19 :: Word32
    , _ox1A :: Word32
    }
makeFields ''OxF

data Ox10 = Ox10
    { _ox0 :: Word32
    , _ox1 :: Blob
    , _ox2 :: Blob
    , _ox3 :: Word32
    , _ox4 :: Word32
    , _ox5 :: Word32
    , _ox6 :: Blob
    , _ox7 :: Blob
    , _ox8 :: Blob
    , _ox9 :: Word32
    , _oxA :: Word32
    , _oxB :: Word32
    , _oxC :: Blob
    , _oxD :: Blob
    , _oxE :: Word32
    , _oxF :: Word32
    , _ox10 :: Word32
    , _ox11 :: Word32
    , _ox12 :: Word32
    , _ox13 :: Word32
    , _ox14 :: Word32
    , _ox15 :: Word32
    , _ox16 :: Word32
    , _ox17 :: Word32
    , _ox18 :: Word32
    , _ox19 :: Word32
    , _ox1A :: Word32
    }
makeFields ''Ox10

data Ox80001000 = Ox80001000
    { _alis :: Blob
    , _cenc :: Word32
    , _ctyp :: Word32
    , _hpky :: Blob
    , _issu :: Blob
    , _labl :: Blob
    , _skid :: Maybe Blob
    , _snbr :: Blob
    , _subj :: Blob
    }
makeFields ''Ox80001000

data Key
  = Kgenp GENP
  | Kinet INET
  | K0xF OxF
  | K0x10 Ox10
  | K0x80001000 Ox80001000
  | KUnknown Blob (HashMap Blob AttributeType)
makePrisms ''Key

data Keychain = Keychain
  { _path :: FilePath
  , _version :: Word
  , _key :: Key
  }
makeLenses ''Keychain

