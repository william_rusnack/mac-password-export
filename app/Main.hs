module Main where

import Text.Megaparsec
import Mac.Keychain.Parse


main :: IO ()
main = do
  let fp = "~/Downloads/keychain.txt"
  fd <- readFile fp
  parse parseKeychain fp fd

