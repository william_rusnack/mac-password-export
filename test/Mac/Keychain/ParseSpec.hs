module Mac.Keychain.ParseSpec where

import Text.Ascii (ascii)
import Data.Char (toUpper)
import Data.Time.Format
import Mac.Keychain.Parse
import Mac.Keychain.Types
import Numeric (showHex, showOct)
import Prelude hiding (null)
import Test.Hspec
-- import Test.QuickCheck
import Text.Megaparsec
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BS8
import qualified Test.Hspec.SmallCheck as SC
import qualified Test.SmallCheck as SC


type Year = Word
type Month = Word
type Day = Word

newDate :: MonadFail m => Year -> Month -> Day -> m Date
newDate y m d = Date <$> parseTimeM True defaultTimeLocale "%F"
  (fpz 4 y <> "-" <> fpz 2 m <> "-" <> fpz 2 d)
  where fpz l = frontPad '0' l . show

frontPad :: Char -> Int -> String -> String
frontPad p l s | l > length s = p : frontPad p (l-1) s
               | otherwise = s

spec :: Spec
spec = do
  describe "date" do
    it "only data" do
      let d = "\"20231122145855Z\\000\""
      t <- newDate 2023 11 22
      parse @String (snd date) "" d `shouldBe` Right t

    it "with hash" do
      let d = "0x32300000000000000000053835355A00  \"20200306145855Z\\000\""
      t <- newDate 2020 03 06
      parse @String (snd date) "" d `shouldBe` Right t

  describe "blob" do
    it "only data" do
      parse @String (snd blob) "" "0x0000000000000000" `shouldBe`
        Right (HexBlob 0 "")
      parse @String (snd blob) "" "0x0000000000000000 \"\"" `shouldBe`
        Right (HexBlob 0 "")
      parse @String (snd blob) "" "0x0000000000000000 \"a\"" `shouldBe`
        Right (HexBlob 0 "a")
      parse @String (snd blob) "" "0x0000000000000001 \"\\000\"" `shouldBe`
        Right (HexBlob 1 $ BS.pack [0])
      parse @String (snd blob) "" "0x0000000000000001 \"a\\000\"" `shouldBe`
        Right (HexBlob 1 $ BS.pack [ascii 'a', 0])
      parse @String (snd blob) "" "0x0000000000000001 \"\\000a\"" `shouldBe`
        Right (HexBlob 1 $ BS.pack [0, ascii 'a'])
      -- 303 = 0xC3, 251 = 0xA9
      parse @String (snd blob) "" "0x4360000000000000096F  \"Caf\\303\\251 Good\"" `shouldBe`
        Right (HexBlob 0x4360000000000000096F ("Caf" <> BS.pack [0xC3, 0xA9] <> " Good"))
      -- 134 = 0x5C
      parse @String (snd blob) "" "0x6D60000000000000000000000000000000006B  \"something\\134gogo\"" `shouldBe`
        Right (HexBlob 0x6D60000000000000000000000000000000006B ("something" <> BS.pack [0x5C] <> "gogo"))
      parse @String (snd blob) "" "0x7B0000000000000000000000000000000000000000000000000000000000000000000000000D00  \"{8000c002-0009-1004-800a-3000b0000002}\\000\"" `shouldBe`
        Right (HexBlob 0x7B0000000000000000000000000000000000000000000000000000000000000000000000000D00 $ "{8000c002-0009-1004-800a-3000b0000002}" `BS.snoc` 0)
      parse @String (snd blob) "" "\"somewebsite.com\"" `shouldBe`
        Right (Blob "somewebsite.com")

  describe "utf8" do
    it "samples" do
      parse @String utf8 "" "\"\"" `shouldBe` Right ""
      parse @String utf8 "" "\"a\"" `shouldBe` Right "a"
      parse @String utf8 "" "\"\\000\"" `shouldBe` Right (BS.pack [0])
      parse @String utf8 "" "\"a\\000\"" `shouldBe` Right (BS.pack [ascii 'a', 0])
      parse @String utf8 "" "\"\\000a\"" `shouldBe` Right (BS.pack [0, ascii 'a'])
      parse @String utf8 "" "\"abc\"" `shouldBe` Right "abc"
      parse @String utf8 "" "\"\\000\\001\\002\"" `shouldBe` Right (BS.pack [0,1,2])
      parse @String utf8 "" "\" \"" `shouldBe` Right " "

  describe "hex" do
    it "samples" do
      parse @String hex "" "0x0" `shouldBe` Right (0x0 :: Word)
      parse @String hex "" "0x1" `shouldBe` Right (0x1 :: Word)
      parse @String hex "" "0x01" `shouldBe` Right (0x01 :: Word)
      parse @String hex "" "0x012" `shouldBe` Right (0x012 :: Word)
      parse @String hex "" "0x1234" `shouldBe` Right (0x1234 :: Word)
      parse @String hex "" "0x1234" `shouldBe` Right (0x1234 :: Integer)

    it "property" $ SC.changeDepth (const 0xFFF) $ SC.property \(x :: Word) -> do
      let bsNum = BS8.pack $ toUpper <$> showHex x mempty
      let zeroPad y = "0x" <> BS8.replicate y '0' <> bsNum
      parse @String hex "" (zeroPad 0) `shouldBe` Right x
      parse @String hex "" (zeroPad 1) `shouldBe` Right x
      parse @String hex "" (zeroPad 5) `shouldBe` Right x

  describe "oct" do
    it "samples" do
      parse @String (oct 1) "" "\\0" `shouldBe` Right (0 :: Word)
      parse @String (oct 1) "" "\\3" `shouldBe` Right (3 :: Word)
      parse @String (oct 3) "" "\\000" `shouldBe` Right (0 :: Word)
      parse @String (oct 3) "" "\\010" `shouldBe` Right (8 :: Word)
      parse @String (oct 3) "" "\\011" `shouldBe` Right (9 :: Word)

    it "property" $ SC.changeDepth (const 0xFFF) $ SC.property \(x :: Word) -> do
      let bsNum = BS8.pack $ toUpper <$> showOct x mempty
      let l = BS8.length bsNum
      let zeroPad y = "\\" <> BS8.replicate y '0' <> bsNum
      parse @String (oct $ l + 0) "" (zeroPad 0) `shouldBe` Right x
      parse @String (oct $ l + 1) "" (zeroPad 1) `shouldBe` Right x
      parse @String (oct $ l + 5) "" (zeroPad 5) `shouldBe` Right x
      

  describe "wordStr" do
    it "only data" do
      let d = "\"abcd\""
      parse @String (snd wordStr) "" d `shouldBe` Right (Word32Str 0x61626364)

    it "with hash" do
      let d = "0x1234ABCD  \"abcd\""
      parse @String (snd wordStr) "" d `shouldBe` Right (Word32Str 0x61626364)

  describe "parseGENP" do
    it "specific" do
      let fn = "test/Mac/Keychain/Parse/data/GENP.txt"
      fd <- BS.readFile fn
      d <- Date <$> parseTimeM True defaultTimeLocale "%Y%m%d" "20151030"
      parse (parseGENP @String) fn fd `shouldBe`
        Right GENP 
          { _ox7  = "com.apple.other.xpc"
          , _ox8  = Nothing
          , _acct = Just "com.apple.other.xpc"
          , _cdat = d
          , _crtr = Just $ "aapl"
          , _cusi = Nothing
          , _desc = Nothing
          , _gena = Nothing
          , _icmt = Nothing
          , _invi = Nothing
          , _mdat = d
          , _nega = Nothing
          , _prot = Nothing
          , _scrp = Nothing
          , _svce = Just "com.apple.other.xpc"
          , _type = Nothing
          }

