module Mac.Keychain.TypesSpec where

import Mac.Keychain.Types
import Test.Hspec
import Data.String


spec :: Spec
spec = do
  describe "Word32Str" do
    let abcdHex = 0x61626364
    let abcd = "abcd"

    it "Show" do
      show (Word32Str abcdHex) `shouldBe` abcd

    it "IsString" do
      fromString abcd `shouldBe` Word32Str abcdHex

